Small change to service.sh: I create the pid file programmatically, since spring boot will fork a new process upon execution - hope that's alright.

I store the data as a map, mapping each station to its associated routes.