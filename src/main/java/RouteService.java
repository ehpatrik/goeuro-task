import java.io.File;
import java.io.IOException;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.stereotype.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.boot.system.ApplicationPidFileWriter;


@RestController
@RequestMapping("api")
@EnableAutoConfiguration
public class RouteService {
	static Routes routes;
	
	public static void main(String[] args) throws IOException{
		 if(args.length == 0){
		        System.out.println("usage: java -jar myservice.jar filename");
		        System.exit(0);
		}

		// read file and initialise data structures
	    routes = new Routes();
	    routes.initialize(new File(args[0]));

	   	// specify PID file and start service
	    SpringApplication sApp = new SpringApplication(RouteService.class);
	    sApp.addListeners(
		new ApplicationPidFileWriter("/tmp/myservice.pid"));
	    sApp.run();
	}

    @RequestMapping(value = "direct", method = RequestMethod.GET, produces = "application/json")
	public Response isDirectPath(@RequestParam(value="dep_sid") int dep_sid,
								 @RequestParam(value="arr_sid")  int arr_sid) {
		
		boolean connected = routes.isConnected(dep_sid, arr_sid);
		return new Response(dep_sid, arr_sid, connected);
	}
}
