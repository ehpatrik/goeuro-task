	
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class Routes {

	/* stationRoutes: 
	 * Maps a station ID (SID) to the set of routes where it occurs.
	 * This structure would require, at worst, O(N*M) Integer entries,
	 * where N is the number of stations and M is the number of routes.
	 * My reasoning is that under normal circumstances, most station would be part of a
	 * limited number of routes (far below the total set of routes), so and thus 
	 * M should be a smaller value.
	 * */
	private HashMap<Integer, Set<Integer>> stationRoutes;
	private int numRoutes = 0;

	public Routes(){}
	
	// Read data from file, and initialize internal data structures.
	public void initialize(File f) throws IOException{
		stationRoutes = new HashMap<Integer, Set<Integer>>();
		importData(f);
	}
	
	
	private void importData(File f) throws IOException{
		int routeID = 0, stationID = 0;
		String line;
		String[] integers;
		Set<Integer> routes;
		
		try(BufferedReader br = new BufferedReader(new FileReader(f))){
			numRoutes = Integer.parseInt(br.readLine());
			
			// Parse a line
			while((line = br.readLine()) != null){
				integers = line.split(" ");
				routeID = Integer.parseInt(integers[0]);
				
				/* Step through each integer, insert route IDs associated with
				*  each station into stationRoutes.
				*/
				for(String s : Arrays.copyOfRange(integers, 1, integers.length)){
					stationID = Integer.parseInt(s);
					if (stationRoutes.containsKey(stationID)){
			        	stationRoutes.get(stationID).add(routeID);
			        }
			        else{
			        	routes = new HashSet<Integer>();
			        	routes.add(routeID);
			        	stationRoutes.put(stationID, routes);
			        }
				}
			}
		}
	}
	
	
	
	public boolean isConnected(int dep, int arr){
		Set<Integer> depRoutes = stationRoutes.get(dep);
		Set<Integer> arrRoutes = stationRoutes.get(arr);
		
		// Invalid SID
		if (depRoutes == null || arrRoutes == null)
			return false;
		
		// Check whether dep and arr co-occur on some route
		return !Collections.disjoint(depRoutes, arrRoutes);
		
	}
	
	public boolean isValidSID(int SID){
		return stationRoutes.containsKey(SID);
	}
	

}